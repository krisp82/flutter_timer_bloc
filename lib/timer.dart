import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_timer/main_bloc.dart';
import 'package:flutter_timer/timer_actions.dart';
import 'package:flutter_timer/timer_background.dart';

class Timer extends StatelessWidget {
  static const TextStyle timerTextStyle = TextStyle(
    fontSize: 60,
    fontWeight: FontWeight.bold,
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Flutter Timer')),
        body: Stack(
          children: <Widget>[
            TimerBackground(),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 100.0),
                  child: Center(
                    child: BlocBuilder<TimerBloc, TimerState>(
                      builder: (context, state) {
                        final String minuteStr = ((state.duration / 60) % 60)
                            .floor()
                            .toString()
                            .padLeft(2, '0');
                        final String secondStr = (state.duration % 60)
                            .floor()
                            .toString()
                            .padLeft(2, '0');
                        return Text('$minuteStr:$secondStr',
                            style: Timer.timerTextStyle);
                      },
                    ),
                  ),
                ),
                BlocBuilder<TimerBloc, TimerState>(
                  condition: (previousState, currentState) =>
                      currentState.runtimeType != previousState.runtimeType,
                  builder: (context, state) => TimerActions(),
                )
              ],
            ),
          ],
        ));
  }
}
